<?php

declare(strict_types=1);

namespace CoStack\FalSftp\Environment;

use CoStack\FalSftp\Driver\SftpDriver;
use phpseclib\Net\SSH2;
use phpseclib3\Net\SFTP;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

use function class_exists;
use function function_exists;

class Detector
{
    private const PHPSECLIB_SSH_CLASS = SSH2::class;
    private const PHPSECLIB3_SSH_CLASS = SFTP::class;
    private const PHPSSH_FUNCTION = 'ssh2_connect';

    public function isPhpseclibAvailable(): bool
    {
        return class_exists(self::PHPSECLIB_SSH_CLASS);
    }

    public function isPhpseclib3Available(): bool
    {
        return class_exists(self::PHPSECLIB3_SSH_CLASS);
    }

    public function isPhpSshAvailable(): bool
    {
        return function_exists(self::PHPSSH_FUNCTION);
    }

    public function getItemsForAdapterSelection(array $params): void
    {
        if ($this->isPhpSshAvailable()) {
            $params['items'][SftpDriver::ADAPTER_PHPSSH] = [
                LocalizationUtility::translate('flexform.general.adapter.phpSshAdapter', 'fal_sftp'),
                SftpDriver::ADAPTER_PHPSSH,
            ];
        }
        if ($this->isPhpseclibAvailable()) {
            $params['items'][SftpDriver::ADAPTER_PHPSECLIB] = [
                LocalizationUtility::translate('flexform.general.adapter.phpseclibAdapter', 'fal_sftp'),
                SftpDriver::ADAPTER_PHPSECLIB,
            ];
        }
        if ($this->isPhpseclib3Available()) {
            $params['items'][SftpDriver::ADAPTER_PHPSECLIB3] = [
                LocalizationUtility::translate('flexform.general.adapter.phpseclib3Adapter', 'fal_sftp'),
                SftpDriver::ADAPTER_PHPSECLIB3,
            ];
        }
    }
}
