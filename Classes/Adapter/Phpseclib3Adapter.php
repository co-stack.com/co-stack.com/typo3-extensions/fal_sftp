<?php

namespace CoStack\FalSftp\Adapter;

use LogicException;
use phpseclib3\Crypt\RSA;
use phpseclib3\Net\SFTP;
use phpseclib3\Net\SSH2;

use function class_exists;
use function file_get_contents;

/**
 * @property SFTP $sftp
 * @property SSH2 $ssh
 */
class Phpseclib3Adapter extends AbstractPhpseclibAdapter
{
    protected function assertClassExist(): void
    {
        if (!class_exists(SFTP::class) || !class_exists(SSH2::class)) {
            throw new LogicException('Can not use phpseclib3 adapter when package is not available', 1616406023);
        }
    }

    protected function createSSH2(): SSH2
    {
        return new SSH2(
            $this->configuration['hostname'],
            $this->configuration['port']
        );
    }

    protected function createSFTP(): SFTP
    {
        return new SFTP(
            $this->configuration['hostname'],
            $this->configuration['port']
        );
    }

    protected function createKeyAuth(): RSA
    {
        return RSA::load(
            file_get_contents($this->configuration['privateKey']),
            $this->configuration['privateKeyPassword'] ?: false
        );
    }

    public function uploadFile(string $source, string $identifier): bool
    {
        return $this->sftp->put($identifier, $source, SFTP::SOURCE_LOCAL_FILE);
    }

    protected function getFileDetails(string $identifier): array
    {
        $details = [];
        $details['size'] = $this->sftp->filesize($identifier);
        $details['atime'] = $this->sftp->fileatime($identifier);
        $details['mtime'] = $this->sftp->filemtime($identifier);
        return $details;
    }

    public function __destruct()
    {
        if ($this->ssh instanceof SSH2) {
            $this->ssh->disconnect();
        }
        if ($this->sftp instanceof SFTP) {
            $this->sftp->disconnect();
        }
    }
}
