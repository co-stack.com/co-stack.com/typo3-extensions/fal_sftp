<?php

namespace CoStack\FalSftp\Adapter;

use CoStack\FalSftp\Driver\SftpDriver;
use phpseclib\Crypt\RSA as RSAv2;
use phpseclib\Net\SFTP as SFTPv2;
use phpseclib\Net\SSH2 as SSH2v2;
use phpseclib3\Crypt\RSA as RSAv3;
use phpseclib3\Net\SFTP as SFTPv3;
use phpseclib3\Net\SSH2 as SSH2v3;
use TYPO3\CMS\Core\Resource\Exception\InvalidConfigurationException;
use TYPO3\CMS\Core\Type\File\FileInfo;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function array_map;
use function base64_decode;
use function decoct;
use function escapeshellarg;
use function explode;
use function in_array;
use function is_array;
use function md5;
use function sha1;
use function str_split;
use function strrpos;
use function strtolower;
use function substr;

abstract class AbstractPhpseclibAdapter extends AbstractAdapter
{
    /** @var null|SFTPv2|SFTPv3| */
    protected $ssh;

    /** @var null|SSH2v2|SSH2v3| */
    protected $sftp;

    protected const WRITABLE = 2;
    protected const READABLE = 4;

    protected $configuration = [];

    protected $info = [
        'userId' => 0,
        'groupIds' => [],
    ];

    public function __construct(array $configuration)
    {
        $this->assertClassExist();
        $this->configuration = $configuration;
    }

    abstract protected function assertClassExist(): void;

    public function connect(): bool
    {
        $this->ssh = $this->createSSH2();

        $authenticationMethod = $this->configuration[SftpDriver::CONFIG_AUTHENTICATION_METHOD];
        if (static::AUTHENTICATION_PASSWORD === (int)$authenticationMethod) {
            $authentication = $this->configuration['password'];
        } elseif (static::AUTHENTICATION_PUBKEY === (int)$authenticationMethod) {
            $authentication = $this->createKeyAuth();
        } else {
            throw new InvalidConfigurationException('Wrong authentication type for phpseclib3Adapter', 1476626149);
        }

        $sshConnected = $this->ssh->login($this->configuration['username'], $authentication);

        if ($sshConnected) {
            $this->sftp = $this->createSFTP();
            $sftpConnected = $this->sftp->login($this->configuration['username'], $authentication);
            if ($sftpConnected) {
                $this->info['userId'] = (int)$this->ssh->exec('echo $EUID');
                $this->info['groupIds'] = GeneralUtility::intExplode(' ', $this->ssh->exec('echo ${GROUPS[*]}'), true);
                return true;
            }
        }
        return false;
    }

    /** @return SSH2v2|SSH2v3 */
    abstract protected function createSSH2();

    /** @return SFTPv2|SFTPv3 */
    abstract protected function createSFTP();

    /** @return RSAv2|RSAv3 */
    abstract protected function createKeyAuth();

    public function getForeignKeyFingerprint(string $hashingMethod)
    {
        switch ($hashingMethod) {
            case self::HASHING_SHA1:
                $hashingMethod = function ($string) {
                    return sha1($string);
                };
                break;
            case self::HASHING_MD5:
            default:
                $hashingMethod = function ($string) {
                    return md5($string);
                };
        }
        $foreignPublicKey = explode(' ', $this->ssh->getServerPublicHostKey());
        return $hashingMethod(base64_decode($foreignPublicKey[1]));
    }

    public function getDetails(string $identifier): array
    {
        $details = $this->getFileDetails($identifier);

        // ctime is not returned by phpseclinb so we just use mtime
        // because it will mostly be the same.
        // @see http://www.linux-faqs.info/general/difference-between-mtime-ctime-and-atime
        $details['ctime'] = $details['mtime'];

        $mimeType = false;

        if ($this->sftp->is_file($identifier)) {
            $fileExtMapping = $GLOBALS['TYPO3_CONF_VARS']['SYS']['FileInfo']['fileExtensionToMimeType'];
            $lcFileExtension = strtolower(substr($identifier, strrpos($identifier, '.') + 1));
            if (!empty($fileExtMapping[$lcFileExtension])) {
                $mimeType = $fileExtMapping[$lcFileExtension];
            }
        }

        if (
            false === $mimeType
            && isset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][FileInfo::class]['mimeTypeGuessers'])
            && is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][FileInfo::class]['mimeTypeGuesser'])
        ) {
            $mimeTypeGuessers = $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][FileInfo::class]['mimeTypeGuesser'];
            foreach ($mimeTypeGuessers as $mimeTypeGuesser) {
                $hookParameters = ['mimeType' => &$mimeType];

                GeneralUtility::callUserFunction(
                    $mimeTypeGuesser,
                    $hookParameters,
                    $this
                );
            }
        }
        $details['mimetype'] = $mimeType;

        return $details;
    }

    abstract protected function getFileDetails(string $identifier): array;

    public function scanDirectory(
        string $identifier,
        bool $files = true,
        bool $folders = true,
        bool $recursive = false
    ): array {
        $directoryEntries = [];
        if (!$files && !$folders) {
            return $directoryEntries;
        }
        $items = $this->sftp->nlist($identifier, $recursive);
        if (false === $items) {
            // Access not allowed?
            return [];
        }
        foreach ($items as $item) {
            if ($item === '.' || $item === '..') {
                continue;
            }
            $itemIdentifier = $identifier . $item;
            if ($files && $this->sftp->is_file($itemIdentifier)) {
                $directoryEntries[$itemIdentifier] = $this->getShortInfo($identifier, 'file');
            } elseif ($folders && $this->sftp->is_dir($itemIdentifier)) {
                $directoryEntries[$itemIdentifier] = $this->getShortInfo($identifier, 'dir');
            }
        }
        return $directoryEntries;
    }

    public function exists(string $identifier, string $type = self::TYPE_FILE): bool
    {
        if ($type === self::TYPE_FILE) {
            return $this->sftp->is_file($identifier);
        }

        if ($type === self::TYPE_FOLDER) {
            return $this->sftp->is_dir($identifier);
        }

        return false;
    }

    public function createFolder(string $identifier, bool $recursive = true): string
    {
        $this->sftp->mkdir($identifier, $this->configuration[SftpDriver::CONFIG_FOLDER_MODE], $recursive);
        return $identifier;
    }

    public function getPermissions(string $identifier): array
    {
        $permissions = [
            'r' => false,
            'w' => false,
        ];

        $filePerms = array_map(
            function ($var) {
                return (int)$var;
            },
            str_split(substr(decoct($this->sftp->fileperms($identifier)), -3, 3))
        );

        if ($this->info['userId'] === $this->sftp->fileowner($identifier)) {
            $permissions['r'] = ($filePerms[0] & self::READABLE) === self::READABLE;
            $permissions['w'] = ($filePerms[0] & self::WRITABLE) === self::WRITABLE;
        }
        if (in_array($this->sftp->filegroup($identifier), $this->info['groupIds'])) {
            $permissions['r'] = $permissions['r'] ?: ($filePerms[1] & self::READABLE) === self::READABLE;
            $permissions['w'] = $permissions['w'] ?: ($filePerms[1] & self::WRITABLE) === self::WRITABLE;
        }
        $permissions['r'] = $permissions['r'] ?: ($filePerms[2] & self::READABLE) === self::READABLE;
        $permissions['w'] = $permissions['w'] ?: ($filePerms[2] & self::WRITABLE) === self::WRITABLE;
        return $permissions;
    }

    public function hash(string $identifier, string $hashAlgorithm): string
    {
        switch ($hashAlgorithm) {
            case 'sha1':
                return substr($this->ssh->exec('sha1sum ' . escapeshellarg($identifier)), 0, 40);
            case 'md5':
                return substr($this->ssh->exec('md5sum ' . escapeshellarg($identifier)), 0, 32);
            default:
        }
        return '';
    }

    public function downloadFile(string $identifier, string $target): string
    {
        return ($this->sftp->get($identifier, $target) === true) ? $target : '';
    }

    public function dumpFile(string $identifier): void
    {
        echo $this->sftp->get($identifier);
    }

    public function delete(string $identifier, bool $recursive): bool
    {
        return $this->sftp->delete($identifier, $recursive);
    }

    public function readFile(string $identifier): string
    {
        return $this->sftp->get($identifier);
    }

    public function rename(string $oldIdentifier, string $newIdentifier): bool
    {
        if ($this->exists($newIdentifier)) {
            $this->delete($newIdentifier, true);
        }
        return $this->sftp->rename($oldIdentifier, $newIdentifier);
    }

    public function copy(string $sourceIdentifier, string $targetIdentifier): bool
    {
        $result = $this->ssh->exec(
            'cp -RPf ' . escapeshellarg($sourceIdentifier) . ' ' . escapeshellarg($targetIdentifier)
        );
        return $result === '';
    }
}
