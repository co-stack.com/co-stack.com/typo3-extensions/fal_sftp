<?php

declare(strict_types=1);

namespace CoStack\FalSftp\Adapter;

use CoStack\FalSftp\Driver\SftpDriver;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RuntimeException;
use SplFileInfo;
use TYPO3\CMS\Core\Type\File\FileInfo;

use function copy;
use function decoct;
use function explode;
use function fclose;
use function file_exists;
use function file_get_contents;
use function in_array;
use function is_dir;
use function is_file;
use function md5_file;
use function readfile;
use function sha1_file;
use function ssh2_auth_password;
use function ssh2_auth_pubkey_file;
use function ssh2_connect;
use function ssh2_exec;
use function ssh2_fetch_stream;
use function ssh2_fingerprint;
use function ssh2_scp_recv;
use function ssh2_scp_send;
use function ssh2_sftp;
use function ssh2_sftp_mkdir;
use function ssh2_sftp_rename;
use function ssh2_sftp_rmdir;
use function ssh2_sftp_unlink;
use function stat;
use function str_replace;
use function stream_get_contents;
use function stream_set_blocking;
use function strlen;
use function substr;

use const SSH2_FINGERPRINT_MD5;
use const SSH2_FINGERPRINT_SHA1;
use const SSH2_STREAM_STDIO;

class PhpSshAdapter extends AbstractAdapter
{
    protected const READ = 4;
    protected const WRITE = 2;

    /**
     * @var string[]|int[]
     */
    protected $configuration = [];

    /** @var null|resource */
    protected $ssh;

    /** @var null|resource */
    protected $sftp;

    protected $sftpWrapper = '';

    protected $sftpWrapperLength = 0;

    protected $iteratorFlags = 0;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    public function connect(): bool
    {
        $this->ssh = ssh2_connect(
            $this->configuration['hostname'],
            (int)$this->configuration['port']
        );

        $username = $this->configuration['username'];

        switch ($this->configuration[SftpDriver::CONFIG_AUTHENTICATION_METHOD]) {
            case static::AUTHENTICATION_PASSWORD:
                ssh2_auth_password($this->ssh, $username, $this->configuration['password']);
                break;
            case static::AUTHENTICATION_PUBKEY:
                $publicKey = $this->configuration['publicKey'];
                $privateKey = $this->configuration['privateKey'];
                if (!file_exists($publicKey) || !file_exists($privateKey)) {
                    return false;
                }
                $password = $this->configuration['privateKeyPassword'];
                if (empty($password)) {
                    $password = null;
                }
                ssh2_auth_pubkey_file($this->ssh, $username, $publicKey, $privateKey, $password);
                break;
            default:
        }
        $this->sftp = ssh2_sftp($this->ssh);
        $this->sftpWrapper = 'ssh2.sftp://' . $this->sftp;
        $this->sftpWrapperLength = strlen($this->sftpWrapper);
        $this->iteratorFlags =
            FilesystemIterator::UNIX_PATHS
            | FilesystemIterator::SKIP_DOTS
            | FilesystemIterator::CURRENT_AS_FILEINFO
            | FilesystemIterator::FOLLOW_SYMLINKS;

        return true;
    }

    public function getForeignKeyFingerprint(string $hashingMethod)
    {
        switch ($hashingMethod) {
            case self::HASHING_SHA1:
                $hashingMethod = SSH2_FINGERPRINT_SHA1;
                break;
            case self::HASHING_MD5:
            default:
                $hashingMethod = SSH2_FINGERPRINT_MD5;
        }
        return ssh2_fingerprint($this->ssh, $hashingMethod);
    }

    public function scanDirectory(
        string $identifier,
        bool $files = true,
        bool $folders = true,
        bool $recursive = false
    ): array {
        $directoryEntries = [];
        $iterator = new RecursiveDirectoryIterator($this->sftpWrapper . $identifier, $this->iteratorFlags);
        while ($iterator->valid()) {
            /** @var $entry SplFileInfo */
            $entry = $iterator->current();
            $identifier = substr($entry->getPathname(), $this->sftpWrapperLength);
            if ($files && $entry->isFile()) {
                $directoryEntries[$identifier] = $this->getShortInfo($identifier, 'file');
            } elseif ($folders && $entry->isDir()) {
                $directoryEntries[$identifier] = $this->getShortInfo($identifier, 'dir');
            }
            $iterator->next();
        }
        if ($recursive) {
            foreach ($directoryEntries as $directoryEntry) {
                if ($directoryEntry['type'] === 'dir') {
                    $scanResults = $this->scanDirectory($directoryEntry['identifier'], $files, $folders, $recursive);
                    foreach ($scanResults as $identifier => $info) {
                        $directoryEntries[$identifier] = $info;
                    }
                }
            }
        }
        return $directoryEntries;
    }

    public function exists(string $identifier, string $type = self::TYPE_FILE): bool
    {
        $identifier = $this->sftpWrapper . $identifier;
        if ($type === self::TYPE_FILE) {
            return is_file($identifier);
        } elseif ($type === self::TYPE_FOLDER) {
            return is_dir($identifier);
        }
        return false;
    }

    public function getPermissions(string $identifier): array
    {
        $path = $this->sftpWrapper . $identifier;
        $stat = stat($path);
        $mode = substr(decoct($stat['mode']), -3);
        $executionResource = ssh2_exec($this->ssh, 'id');
        $outputStream = ssh2_fetch_stream($executionResource, SSH2_STREAM_STDIO);
        stream_set_blocking($outputStream, true);
        $output = stream_get_contents($outputStream);
        fclose($outputStream);
        $parts = explode(' ', $output);
        $idParts = [];
        foreach ($parts as $part) {
            [$key, $value] = explode('=', $part);
            $idParts[$key] = $value;
        }
        $uid = (int)$idParts['uid'];
        $pids = [];
        foreach (explode(',', $idParts['groups']) as $gid) {
            $pids[] = (int)$gid;
        }

        $isOwner = (int)$stat['uid'] === $uid;
        $isGroup = in_array($stat['pid'], $pids);

        return [
            'r' => $mode[2] & self::READ
                   || ($isGroup && ($mode[1] & self::READ))
                   || ($isOwner && ($mode[0] & self::READ)),
            'w' => $mode[2] & self::WRITE
                   || ($isGroup && ($mode[1] & self::WRITE))
                   || ($isOwner && ($mode[0] & self::WRITE)),
        ];
    }

    public function createFolder(string $identifier, bool $recursive = true): string
    {
        ssh2_sftp_mkdir($this->sftp, $identifier, $this->configuration[SftpDriver::CONFIG_FOLDER_MODE], $recursive);
        return $identifier;
    }

    public function getDetails(string $identifier): array
    {
        $fileInfo = new FileInfo($this->sftpWrapper . $identifier);
        $details = [];
        $details['size'] = $fileInfo->getSize();
        $details['atime'] = $fileInfo->getATime();
        $details['mtime'] = $fileInfo->getMTime();
        $details['ctime'] = $fileInfo->getCTime();
        $details['mimetype'] = (string)$fileInfo->getMimeType();
        return $details;
    }

    public function hash(string $identifier, string $hashAlgorithm): string
    {
        switch ($hashAlgorithm) {
            case 'sha1':
                return sha1_file($this->sftpWrapper . $identifier);
            case 'md5':
                return md5_file($this->sftpWrapper . $identifier);
            default:
        }
        return '';
    }

    public function downloadFile(string $identifier, string $target): string
    {
        if (ssh2_scp_recv($this->ssh, $identifier, $target)) {
            return $target;
        }
        throw new RuntimeException(
            'Copying file "' . $identifier . '" to temporary path "' . $target . '" failed.',
            1447607200
        );
    }

    public function uploadFile(string $source, string $identifier): bool
    {
        return ssh2_scp_send($this->ssh, $source, $identifier, $this->configuration[SftpDriver::CONFIG_FILE_MODE]);
    }

    public function dumpFile(string $identifier): void
    {
        readfile($this->sftpWrapper . $identifier);
    }

    public function delete(string $identifier, bool $recursive): bool
    {
        if (is_dir($this->sftpWrapper . $identifier)) {
            return ssh2_sftp_rmdir($this->sftp, $identifier);
        } elseif (is_file($this->sftpWrapper . $identifier)) {
            return ssh2_sftp_unlink($this->sftp, $identifier);
        } else {
            return false;
        }
    }

    public function readFile(string $identifier): string
    {
        return file_get_contents($this->sftpWrapper . $identifier);
    }

    public function rename(string $oldIdentifier, string $newIdentifier): bool
    {
        $this->delete($newIdentifier, false);
        if (ssh2_sftp_rename($this->sftp, $oldIdentifier, $newIdentifier)) {
            return true;
        } else {
            $this->delete($oldIdentifier, false);
            return false;
        }
    }

    public function copy(string $sourceIdentifier, string $targetIdentifier): bool
    {
        $success = false;
        $oldIdentifier = $this->sftpWrapper . $sourceIdentifier;
        $newIdentifier = $this->sftpWrapper . $targetIdentifier;
        if (is_dir($oldIdentifier)) {
            $items = $this->scanDirectory($sourceIdentifier, true, true, true);
            foreach ($items as $item) {
                $source = $item['identifier'];
                $target = str_replace($sourceIdentifier, $targetIdentifier, $source);
                if (is_dir($this->sftpWrapper . $source)) {
                    $success = $target === $this->createFolder($target);
                } else {
                    $success = copy($this->sftpWrapper . $source, $this->sftpWrapper . $target);
                }
            }
        } else {
            $success = copy($oldIdentifier, $newIdentifier);
        }
        return $success;
    }

    public function __destruct()
    {
        $this->ssh = null;
    }
}
